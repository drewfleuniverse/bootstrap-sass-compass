#Create a Bootstrap-SASS project with Compass

##Environment

###Operating System
Linux ubuntu 14.04

###Requirement
- Bootstrap SASS Source, downloadable from Bootstrap official website or using Bower to get it.
- Compass

##Recipe

###Create a compass project with Bootstrap SASS source folder inside:
```bash
compass create css
cd css
rm sass/* stylesheets/*
wget https://github.com/twbs/bootstrap-sass/archive/v3.3.4.tar.gz
tar xzvf v3.3.4.tar.gz && rm v3.3.4.tar.gz
```

###Edit the ‘# Set this to the root’ section in ‘config.rb’ as following:
```bash
vim config.rb
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "stylesheets"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "javascripts"
add_import_path = "bootstrap-sass-3.3.4"
```

###Comment out the following line in ‘_bootstrap.scss’ as below:
```bash
vim bootstrap-sass-3.3.4/assets/stylesheets/_bootstrap.scss
//@import "bootstrap/variables";
```

###Setup the files and folder in ‘sass’:
```bash
touch sass/style-custom.scss
mkdir -p sass/custom && touch sass/custom/_app.scss
cp bootstrap-sass-3.3.4/assets/stylesheets/bootstrap/_variables.scss sass/custom
```

###Edit ‘style-custom.scss’ as following:
```baah
vim sass/style-custom.scss
// Custom variables
@import "custom/variables";
// Core variables and mixins
@import "../bootstrap-sass-3.3.4/assets/stylesheets/bootstrap.scss";
// Custom classes
@import "custom/app"
```

###Run the following line to create and watch ‘style-custom.css’ in ‘stylesheets’:
```bash
compass watch
```

##Notes
This method would not include jQuery. If you’d like to install Bootstrap with jQuery in one go, use Bower instead.

###Reference
Compass Compiling and WordPress Themes
Compass with SASS for Team Development
